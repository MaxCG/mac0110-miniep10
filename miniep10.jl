function sum_submatrix(m, tam, a, b)
	sum = 0
	for i in a:a+tam-1
		for j in b:b+tam-1
			sum += m[i, j]
		end
	end
	return sum
end

function max_sum_submatrix(m)
	max_sum = -Inf
	max_arr = []
	for tam in 1:size(m)[1]
		for i in 1:size(m)[1] - tam + 1
			for j in 1:size(m)[2] - tam + 1
				temp = sum_submatrix(m, tam, i, j)
				if temp > max_sum
					max_sum = temp
					max_arr = [ m[i:(i + tam - 1), j:(j + tam -1)] ]
				elseif temp == max_sum
					push!(max_arr, m[i:(i + tam - 1), j:(j + tam -1)])
				end
			end
		end
	end
	return max_arr
end
