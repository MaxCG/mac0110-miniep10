using Test
include("miniep10.jl")

function test()
	println("Testing")
	@test max_sum_submatrix([1 1 1; 1 1 1; 1 1 1]) == [[1 1 1; 1 1 1; 1 1 1]]
	@test max_sum_submatrix([0 -1; -2 -3]) == [hcat(0)]
	@test max_sum_submatrix([-1 1 1 ; 1 -1 1 ; 1 1 -2]) == [[1 1; -1 1], [1 -1; 1 1], [-1 1 1; 1 -1 1; 1 1 -2]]
	println("OK")
end

test()
